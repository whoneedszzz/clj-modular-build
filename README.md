# clj-modular-build

A modular Clojure(Script) build tool, inspired by Leiningen, utilizing the Clojure CLI tools.